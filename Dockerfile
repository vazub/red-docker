FROM i386/ubuntu
RUN apt-get update && apt-get install --no-install-recommends -y wget apt-utils libcanberra-gtk-module libcanberra-gtk3-module libgtk-3-0 libcurl4
WORKDIR /root
RUN ln -s /home /Users
RUN wget --no-check-certificate https://static.red-lang.org/dl/auto/linux/red-latest
RUN chmod u+x red-latest
RUN /root/red-latest
RUN apt-get autoclean && apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*
WORKDIR /tmp
ENTRYPOINT ["/root/red-latest"]