# Red with Docker

Here is the way to use the latest Linux-based snapshot of the [Red programming language](https://github.com/red/red).

When set up correctly, you will be able to use `red-docker` command as a drop-in replacement for the usual standalone **Red** executable in all relevant contexts.

* [Build Docker Image](#build-docker-image)
* [Linux Setup](#linux-setup)
* [MacOS Setup](#macos-setup)
* [Windows 10 Setup](#windows-10-setup)
* [Usage](#usage)
* [Dealing with X11 issues](#dealing-with-x11-issues)
* [Running or compiling Red from sources](#running-or-compiling-red-from-sources)
* [License](#license)

---

## Build Docker Image

```bash
docker build https://codeberg.org/vazub/red-docker.git -t red-docker
```

## Linux Setup

This was tested on vanilla Pop!_OS 21.04 (64-bit), but should work with minimal adjustment on other distros as well.

Make sure that your X server is installed and `echo $DISPLAY` shows valid output, for example something like `:1` or other number. Then run the following command to fix the access control list:

```bash
xhost +local:docker
```

Now, fix your shell dotfile to have the convenience function to run the image:

```bash
echo $'\nfunction red-docker() {\narrVar=()
for arg in "$@"; do
	if 	[[ $arg = -* ]]; then
		arrVar+=($arg)
	else 
		var=$(realpath -eq $arg)
		if
			[[ $var ]]; then
			arrVar+=($var)
		else
			arrVar+=($arg)
		fi	
	fi  
done
	docker run --rm -ti --platform=linux/386 -v /home:/home -v /tmp:/tmp -e DISPLAY=$DISPLAY red-docker ${arrVar[*]}
\n}' >> ~/.bashrc
```

## MacOS Setup

Install **XQuartz** and **socat**. Easiest way is to use [Homebrew](https://brew.sh/):

```bash
brew update && brew install xquartz && brew install socat && brew install coreutils
```

Starting with macOS Catalina, Macs will now use **Zsh** as the default login shell and interactive shell across the operating system. That is why we need to create and/or edit `.zshrc` instead of `.bashrc`.

```bash
echo $'export DISPLAY_MAC=$(ifconfig en0 | grep "inet " | cut -d " " -f2):0' >> ~/.zshrc
```

```bash
echo $'function startx() {\n\tif [ -z "$(ps -ef|grep XQuartz|grep -v grep)" ]; then\n\t\topen -a XQuartz\n\t\tsocat TCP-LISTEN:6000,reuseaddr,fork UNIX-CLIENT:\\\"$DISPLAY\\\" &\n\tfi\n}' >> ~/.zshrc
```

```bash
echo $'\nfunction red-docker() {\narrVar=()
for arg in "$@"; do
	if 	[[ $arg = -* ]]; then
		arrVar+=($arg)
	else 
		var=$(realpath -eq $arg)
		if
			[[ $var ]]; then
			arrVar+=($var)
		else
			arrVar+=($arg)
		fi	
	fi  
done
	docker run --rm -ti --platform=linux/386 -v /Users:/home -v /tmp:/tmp -e DISPLAY=$DISPLAY_MAC red-docker ${arrVar[*]}
\n}
}' >> ~/.zshrc
```

```bash
source ~/.zshrc
```

Start **XQuartz** and **socat**:

```bash
startx
```

## Windows 10 Setup

This setup flow depends on WSL2 and some X11 server (tested with X410) to be installed and running on the host system.

In PowerShell run `wsl` to start the default distro

If using WSL versions of distros with Bash shell, run these commands in sequence:

```bash
echo $'export DISPLAY=$(cat /etc/resolv.conf | grep nameserver | awk \'{print $2}\'):0.0' >> ~/.bashrc
```

```bash
echo 'export LIBGL_ALWAYS_INDIRECT=1' >> ~/.bashrc
```

```bash
echo $'\nfunction red-docker() {\narrVar=()
for arg in "$@"; do
	if 	[[ $arg = -* ]]; then
		arrVar+=($arg)
	else 
		var=$(realpath -eq $arg)
		if
			[[ $var ]]; then
			arrVar+=($var)
		else
			arrVar+=($arg)
		fi	
	fi  
done
	docker run --rm -ti --platform=linux/386 -v /home:/home -v /tmp:/tmp -e DISPLAY=$DISPLAY red-docker ${arrVar[*]}
\n}' >> ~/.bashrc
```

```bash
source ~/.bashrc
```

Working with other default shells should be the same, just replace output redirection in above commants to your shell's session profile.

**IMPORTANT**: Dockerized Red won't currently list files in mounted Windows paths, so if you want to use it as intended by this particular flow, then keep your project files inside WSL2 distro's filesystem, ex. `/home/<user>/<project>`.

More WSL2 insights and tips on how to setup your workflow can be found here:

* [Comparing WSL 1 and WSL 2](https://docs.microsoft.com/en-us/windows/wsl/compare-versions)
* [Developing in WSL](https://code.visualstudio.com/docs/remote/wsl)

## Usage

Run Red with the `red-docker` command. This will start the REPL. Otherwise, you can use it as a drop-in replacement, to run scripts, like this:

```bash
red-docker <your-script>.red
```

## Dealing with X11 Issues

In case you get some errors that indicate some X11 configuration troubles, check your Windows Firewall settings for the X server you are using. In most cases, the easiest solution would be to delete all specified X server rules and when the firewall asks for new permissions on server restart - make sure allow "Public Network" access. If you use X410, [here](https://x410.dev/cookbook/wsl/using-x410-with-wsl2/) is a good guide how to check is everything is working as intended.

## Running or compiling Red from sources

Refer to the Rebol 2 with Docker setup, provided [here](https://codeberg.org/vazub/rebol2-docker).

## License

Use of this source code is governed by the MIT-like permissive [Blue Oak Model License](https://blueoakcouncil.org/license/1.0.0), an exact copy of which can be found in the relevant [LICENSE](./LICENSE.md) file of the current repository.
